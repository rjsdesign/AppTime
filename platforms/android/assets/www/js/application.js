// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
var token;
var id_user;
var logged;
token =localStorage.getItem('token');
user_id =localStorage.getItem('user_id');
logged=localStorage.getItem('logged');
$(document).ready(function(){
    $('input#input_text, textarea#textarea1').characterCounter();
    $('.modal').modal();
    $(".button-collapse").sideNav();

});

 ///////////////////////////////////////////// Date Time://///////////////////////////////////////////
$('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 2, // Creates a dropdown of 15 years to control year
    labelMonthNext: 'Mois suivant',
    labelMonthPrev: 'Mois précédent',
    labelMonthSelect: 'Selectionner le mois',
    labelYearSelect: 'Selectionner une année',
    monthsFull: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
    monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
    weekdaysFull: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
    weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ],
    weekdaysLetter: [ 'D', 'L', 'M', 'Mer', 'J', 'V', 'S' ],

    today: 'Aujourd\'hui',
    clear: 'Réinitialiser',
    close: 'Fermer',
    format: 'dd/mm/yyyy'

});
////////////////////////////// Time Picker: //////////////////////////////////////////////////////////////
$('.timepicker').pickatime({

    default: 'now', // Set default time
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Réinitialiser', // text for clear-button
    canceltext: 'Fermer', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
});

////////////////////////////// Time Picker: //////////////////////////////////////////////////////////////
$('.duration').pickatime({

    default: 'now', // Set default time
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Réinitialiser', // text for clear-button
    canceltext: 'Fermer', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
});







/////////////////////////////////////////  Stopwatch:  ///////////////////////////////////////////////
/*'use strict';

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Stopwatch = function () {
    function Stopwatch(display, results) {
        _classCallCheck(this, Stopwatch);

        this.running = false;
        this.display = display;
        this.results = results;
        this.laps = [];
        this.reset();
        this.print(this.times);
    }

    Stopwatch.prototype.reset = function reset() {
        this.times = [0, 0, 0];
    };

    Stopwatch.prototype.start = function start() {
        if (!this.time) this.time = performance.now();
        if (!this.running) {
            this.running = true;
            requestAnimationFrame(this.step.bind(this));
        }
    };

    Stopwatch.prototype.lap = function lap() {
        var times = this.times;
        //var li = document.createElement('li');
        //li.innerText = this.format(times);
        //this.results.appendChild(li);
        $('duree').value = this.times;

    };

    Stopwatch.prototype.stop = function stop() {
        this.running = false;
        this.time = null;
    };


    Stopwatch.prototype.step = function step(timestamp) {
        if (!this.running) return;
        this.calculate(timestamp);
        this.time = timestamp;
        this.print();
        requestAnimationFrame(this.step.bind(this));
    };

    Stopwatch.prototype.calculate = function calculate(timestamp) {
        var diff = timestamp - this.time;
        // Hundredths of a second are 100 ms
        this.times[2] += diff / 10;
        // Seconds are 100 hundredths of a second
        if (this.times[2] >= 100) {
            this.times[1] += 1;
            this.times[2] -= 100;
        }
        // Minutes are 60 seconds
        if (this.times[1] >= 60) {
            this.times[0] += 1;
            this.times[1] -= 60;
        }
    };

    Stopwatch.prototype.print = function print() {
        this.display.innerText = this.format(this.times);
    };

    Stopwatch.prototype.format = function format(times) {
        return pad0(times[0], 2) + ':' + pad0(times[1], 2) + ':' + pad0(Math.floor(times[2]), 2);
    };

    return Stopwatch;
}();

function pad0(value, count) {
    var result = value.toString();
    for (; result.length < count; --count) {
        result = '0' + result;
    }return result;
}

function clearChildren(node) {
    while (node.lastChild) {
        node.removeChild(node.lastChild);
    }
}

var stopwatch = new Stopwatch(document.querySelector('.stopwatch'), document.querySelector('.results'));
*/
//////////////////////////////////// Modal signature:    //////////////////////////////
/*
$('.modal').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
            alert("Ready");
            console.log(modal, trigger);
        },
        complete: function() { alert('Closed'); } // Callback for Modal close
    }
);
*/