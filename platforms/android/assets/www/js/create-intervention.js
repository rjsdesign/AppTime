/**
 * Created by rjsdesign on 18/07/17.
 */
if (logged == 1) {

    $(document).ready(function () {

        $.ajax({
            url: 'https://fdt.mithril.re/projects.json?user_credentials=' + token,

            type: 'GET',

            dataType: 'json', //Type

            success: function (retour, statut) {

                //Vérification de la requête ajax
                if (statut == "success") {
                    //Vérification du json renvoyé
                    if (!retour) {
                        //Si erreur dans json
                        alert(retour.message);
                    } else {
                        //traitement
                        var o = retour;
                        var test = '<option value=""></option>';
                        var i = 0;
                        console.dir(o[0].customer_id);

                        for (a in o) {
                            test += '<option  value="' + o[i].id + '">' + o[i].full_title + '</option>'
                            ;

                            i = i + 1;//";

                        }
                        console.dir(test);
                        $("#selectP").html(test);
                        $('#selectP').material_select();
                    }
                }
                else {
                    //Si la requête ajax n'est pas executée
                    alert("Erreur impossible de joindre le serveur.");
                }
            }

        });

        document.getElementById('btn_val').onclick = function () {

            var durer =document.getElementById('time').value;
            var parts = durer.split(':');
            var heures= Number(parts[0])*60+Number(parts[1]);
            var date_start =document.getElementById('datepicker').value;
            var time_start =document.getElementById('timepicker').value;
            var date = date_start +"T"+ time_start ;
            var fact = document.getElementById('fact').value;
            var description = document.getElementById('description').value;
            var project = document.getElementById('selectP').value;

            console.log(project);


                        var sheets= {sheet : { start_date : date ,
                             duration: heures ,
                             compta:false ,
                             description:description ,
                             facturable :false ,
                             projet_id :project
                          },user_credentials: token};
                   $.ajax({
                        type: "POST",
                        url: "https://fdt.mithril.re/sheets.json",
                        dataType: "json",
                        data:sheets ,
                            success: function (error) {
                                console.log("ok");
                              },
                              error: function (error){

                                console.log(error);
                               }

                   });

            };
    });
} else {
    document.location.href = "../index.html";
}
;
