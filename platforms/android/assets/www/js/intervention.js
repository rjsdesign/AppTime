/**
 * Created by rjsdesign on 10/07/17.
 */
if(logged==1){
    $(".button-collapse").sideNav();
    $(document).ready(function () {

        $.ajax({

            url: 'https://fdt.mithril.re/sheets.json?user_credentials=gk5MAeYEgpkvVF3O1sJ5',

            type: 'GET',

            dataType: 'json', //Type

            success: function (retour, statut) {

                //Vérification de la requête ajax
                if (statut == "success") {
                    //Vérification du json renvoyé
                    if (!retour) {
                        //Si erreur dans json
                        alert(retour.message);
                    } else {
                        //traitement
                        var o = retour;
                        var test = "";
                        var i = 0;
                        console.dir(o);
                        for (a in o) {

                            test += '<tr><td>' + o[i].customer_name + '</td> <td>' + o[i].user_name + '</td><td>' + o[i].duration + '</td><td>' + o[i].start_date + '</td></tr>';
                            i = i + 1;//";
                        }
                        $("#table-intervention").html(test);
                    }
                }
                else {
                    //Si la requête ajax n'est pas executée
                    alert("Erreur impossible de joindre le serveur.");
                }


            }
        });
    });
}
else {
    document.location.href="../index.html";
}

